//
//  ApiServise.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class ApiService: NSObject {
    
    static let sharedService = ApiService()
    
    func fetchRealEstate(withOffset offset:Int = 0, priceFrom:Int = 0, priceTo:Int = 0, completion: @escaping ([RealEstate]) -> ()) {
        
        let urlString = "http://api.trend-dev.ru/v2/apartments/blocks/search/?" + "offset=\(offset)&count=10&cache=false&show_type=list&price_to=\(priceTo)&price_from=\(priceFrom)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let url = URL(string: urlString) {
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data, responce, error) in
                
                if error != nil {
                    print(error!)
                    return
                }
                
                let estates = self.serializeEstates(usingData: data)
                completion(estates)
                
            }).resume()
        }
    }
    
    private func serializeEstates(usingData data: Data?) -> [RealEstate] {
        
        var estates = [RealEstate]()
        
        guard let data = data else { return estates }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : AnyObject]
            
            if let dataString = json["data"] as? [String : AnyObject] {
                
                if let results = dataString["results"] as? [[String: AnyObject]] {
                    
                    for placeDict in results {
                        
                        let estate = RealEstate()
                        
                        estate.name = placeDict["name"] as? String
                        estate.deadline = placeDict["deadline"] as? String
                        estate.imageUrl = placeDict["image"] as? String
                        estate.region = placeDict["region"]?["name"] as? String
                        estate.builder = placeDict["builder"]?["name"] as? String
                        
                        if let subways = placeDict["subways"] as? [[String: AnyObject]] {
                            
                            for subwayDict in subways {
                                
                                let subway = Subway()
                                subway.name = subwayDict["name"] as? String
                                subway.distanceTiming = subwayDict["distance_timing"] as? Int
                                
                                estate.subways.append(subway)
                            }
                        }
                        
                        if let apartments = placeDict["min_prices"] as? [[String: AnyObject]] {
                            
                            for apartmentDict in apartments {
                                
                                let apartment = Apartment()
                                apartment.rooms = apartmentDict["rooms"] as? String
                                apartment.totalArea = apartmentDict["totalArea"] as? String
                                apartment.price = apartmentDict["price"] as? Int
                                
                                estate.minPrices.append(apartment)
                            }
                        }
                        
                        estates.append(estate)
                    }
                }
            }
            
        } catch let error {
            print(error)
        }
        
        return estates
    }
    
}
