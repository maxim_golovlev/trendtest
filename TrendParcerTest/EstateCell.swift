//
//  EstateCell.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class EstateCell : BaseCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Bold", size: 18)
        return label
    }()
    
    let deadlineLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        return label
    }()
    
    let iconImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    //min price key labels
    let minPriceStudioKeyLabel: UILabel = {
        let label = UILabel()
        label.text = "Ст"
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let minPriceOneRoomKeyLabel: UILabel = {
        let label = UILabel()
        label.text = "1-к.кв"
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let minPriceTwoRoomKeyLabel: UILabel = {
        let label = UILabel()
        label.text = "2-к.кв"
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let minPriceThreeRoomKeyLabel: UILabel = {
        let label = UILabel()
        label.text = "3-к.кв"
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    //min price value labels
    let minPriceStudioValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let minPriceOneRoomValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let minPriceTwoRoomValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let minPriceThreeRoomValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    //
    let regionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        return label
    }()
    
    let builderLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.init(name: "Roboto-Bold", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let subwayLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        return label
    }()
    
    let distanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        return label
    }()
    
    var estate: RealEstate? {
        
        didSet {

            if let title = estate?.name {
                nameLabel.text = "ЖК «\(title)»"
            }
            if let deadline = estate?.deadline {
                deadlineLabel.text = "Срок сдачи : \(deadline)"
            }
            
            regionLabel.text = estate?.region
            builderLabel.text = estate?.builder
            
            if let minPrices = estate?.minPrices {
                
                for (index, apartment) in minPrices.enumerated() {
                    
                    if let price = apartment.price, let formattedPrice = priceNumberFormatter.string(from: NSNumber(value: price)) {
                    
                        switch index {
                        case 0:
                            minPriceStudioValueLabel.text = "от " + formattedPrice + " руб."
                        case 1:
                            minPriceOneRoomValueLabel.text = "от " + formattedPrice + " руб."
                        case 2:
                            minPriceTwoRoomValueLabel.text = "от " + formattedPrice + " руб."
                        case 3:
                            minPriceThreeRoomValueLabel.text = "от " + formattedPrice + " руб."
                        default:
                            break
                        }
                    }
                }
            }
            
            iconImageView.loadImageUsingUrlString(estate?.imageUrl)
            
            let metroIcon = NSTextAttachment()
            metroIcon.image = #imageLiteral(resourceName: "metro_icon")
            
            let busIcon = NSTextAttachment()
            busIcon.image = #imageLiteral(resourceName: "bus_icon")
            
            let shortestSubway = estate?.subways.sorted { $0.0.distanceTiming! < $0.1.distanceTiming! }.first
            
            if let name = shortestSubway?.name, let time = shortestSubway?.distanceTiming {
                
                let combine = NSMutableAttributedString()
                combine.append(NSAttributedString(attachment: metroIcon))
                combine.append(NSAttributedString(string: " "))
                combine.append(NSAttributedString(string: name))
                combine.append(NSAttributedString(string: "   "))
                combine.append(NSAttributedString(attachment: busIcon))
                combine.append(NSAttributedString(string: " "))
                combine.append(NSAttributedString(string: "\(time)" + " мин."))
                
                subwayLabel.attributedText = combine
            }
        }
    }
    
    override func setupViews() {
        
        backgroundColor = .white
        
        addSubview(nameLabel)
        addSubview(deadlineLabel)
        addSubview(iconImageView)
        addSubview(regionLabel)
        addSubview(subwayLabel)
        
        addConstraintsWithFormat("H:|-20-[v0]-20-|", views: nameLabel)
        addConstraintsWithFormat("H:|-20-[v0]-20-|", views: deadlineLabel)
        addConstraintsWithFormat("H:|-20-[v0(120)]", views: iconImageView)
        addConstraintsWithFormat("H:|-20-[v0]", views: regionLabel)
        addConstraintsWithFormat("H:|-20-[v0]", views: subwayLabel)
        
        addConstraintsWithFormat("V:|-10-[v0]-5-[v1]-10-[v2(90)]-15-[v3]-10-[v4]", views: nameLabel, deadlineLabel, iconImageView, regionLabel, subwayLabel)
        //keys
        addSubview(minPriceStudioKeyLabel)
        minPriceStudioKeyLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 10).isActive = true
        minPriceStudioKeyLabel.topAnchor.constraint(equalTo: iconImageView.topAnchor).isActive = true
        
        addSubview(minPriceOneRoomKeyLabel)
        minPriceOneRoomKeyLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 10).isActive = true
        minPriceOneRoomKeyLabel.topAnchor.constraint(equalTo: minPriceStudioKeyLabel.bottomAnchor, constant: 10).isActive = true
        
        addSubview(minPriceTwoRoomKeyLabel)
        minPriceTwoRoomKeyLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 10).isActive = true
        minPriceTwoRoomKeyLabel.topAnchor.constraint(equalTo: minPriceOneRoomKeyLabel.bottomAnchor, constant: 10).isActive = true
      
        addSubview(minPriceThreeRoomKeyLabel)
        minPriceThreeRoomKeyLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 10).isActive = true
        minPriceThreeRoomKeyLabel.topAnchor.constraint(equalTo: minPriceTwoRoomKeyLabel.bottomAnchor, constant: 10).isActive = true
        
        //values
        addSubview(minPriceStudioValueLabel)
        minPriceStudioValueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        minPriceStudioValueLabel.topAnchor.constraint(equalTo: iconImageView.topAnchor).isActive = true

        addSubview(minPriceOneRoomValueLabel)
        minPriceOneRoomValueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        minPriceOneRoomValueLabel.topAnchor.constraint(equalTo: minPriceStudioValueLabel.bottomAnchor, constant: 10).isActive = true

        addSubview(minPriceTwoRoomValueLabel)
        minPriceTwoRoomValueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        minPriceTwoRoomValueLabel.topAnchor.constraint(equalTo: minPriceOneRoomValueLabel.bottomAnchor, constant: 10).isActive = true

        addSubview(minPriceThreeRoomValueLabel)
        minPriceThreeRoomValueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        minPriceThreeRoomValueLabel.topAnchor.constraint(equalTo: minPriceTwoRoomValueLabel.bottomAnchor, constant: 10).isActive = true
        
        addSubview(builderLabel)
        builderLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        builderLabel.topAnchor.constraint(equalTo: regionLabel.topAnchor).isActive = true
        
    }
}
