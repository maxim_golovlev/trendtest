//
//  EstateFooterCell.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class EstateFooterCell: BaseCell {
    
    lazy var loadButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.init(name: "Roboto-Regular", size: 14)
        button.setTitle("Загрузить еще 10", for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.layer.borderWidth = 0.5
        return button
    }()
    
    var buttonHandler: (() -> ())?
    
    override func setupViews() {
        
        addSubview(loadButton)
        addConstraintsWithFormat("H:|-20-[v0]-20-|", views: loadButton)
        loadButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    func buttonTapped() {
        print("Загрузить еще 10")
        
        if let handler = buttonHandler {
            handler()
        }
    }
}
