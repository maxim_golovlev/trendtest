//
//  EstatePriceCell.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class EstatePriceCell: BaseCell {
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? grayColor : .white
            chechmarkImageView.isHidden = isSelected ? false : true
        }
    }
    
    let pricelabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 13)
        label.alpha = 0.5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let chechmarkImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "checkmark"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isHidden = true
        return imageView
    }()
        
    var price: Int? {
        didSet {
            
            if let price = price, let formattedPrice = priceNumberFormatter.string(from: NSNumber(value: price)) {
                if price == 0 {
                    pricelabel.text = "Сбросить"
                } else {
                    pricelabel.text =  formattedPrice + " руб."
                }
            }
        }
    }
    
    override func setupViews() {
        
        backgroundColor = .white
        
        addSubview(pricelabel)
        pricelabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        pricelabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(chechmarkImageView)
        chechmarkImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        chechmarkImageView.rightAnchor.constraint(equalTo: pricelabel.leftAnchor, constant: -5).isActive = true
    }
}
