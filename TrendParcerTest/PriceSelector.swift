//
//  PriceSelector.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class PriceSelector: UIView {
    
    var priceText: String? {
        didSet {
            priseLabel.text = priceText
        }
    }
    
    let priseLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 13)
        label.alpha = 0.5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        isUserInteractionEnabled = true
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "arrow_down"))
        imageView.setContentHuggingPriority(751, for: .horizontal)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(priseLabel)
        addSubview(imageView)
        
        priseLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        priseLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: rightAnchor , constant: -20).isActive = true
        priseLabel.rightAnchor.constraint(greaterThanOrEqualTo: imageView.leftAnchor, constant: 4).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
