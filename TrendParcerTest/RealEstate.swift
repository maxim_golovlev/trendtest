//
//  RealEstate.swift
//  TrendParcerTest
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class RealEstate {
    
    var name: String?
    var deadline: String?
    var imageUrl: String?
    var region: String?
    var subways = [Subway]()
    var minPrices = [Apartment]()
    var builder: String?
}

class Subway {
    
    var name: String?
    var distanceTiming: Int?
}

class Apartment {
    
    var rooms: String?
    var totalArea: String?
    var price: Int?
}
