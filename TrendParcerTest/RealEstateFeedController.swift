//
//  ViewController.swift
//  TrendParcerTest
//
//  Created by Admin on 14.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RealEstateFeedController: UIViewController {
    
    let estateCellId = "estateCellId"
    let pricesCellId = "pricesCellId"
    let footerId = "footerId"
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var minPriceSelector: PriceSelector = {
        let view = PriceSelector()    
        view.priceText = "Цена от, руб"
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMinPriceTapped)))
        return view
    }()
    
    lazy var maxPriceSelector: PriceSelector = {
        let view = PriceSelector()
        view.priceText = "Цена до, руб"
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMaxPriceTapped)))
        return view
    }()
    
    let sortBar: SortBar = {
        let view = SortBar()
        return view
    }()
    
    lazy var feedCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = grayColor
        cv.dataSource = self
        cv.delegate = self
        cv.register(EstateCell.self, forCellWithReuseIdentifier: self.estateCellId)
        cv.register(EstateFooterCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: self.footerId)
        return cv
    }()
    
    lazy var minPriceCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = grayColor
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.dataSource = self
        cv.delegate = self
        cv.register(EstatePriceCell.self, forCellWithReuseIdentifier: self.pricesCellId)
        let firstIndex = IndexPath(row: 0, section: 0)
        cv.selectItem(at: firstIndex, animated: false, scrollPosition: UICollectionViewScrollPosition())
        return cv
    }()
    
    let minPriceCollectionViewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize(width: -5, height: 5)
        view.layer.shadowRadius = 5
        view.layer.shadowOpacity = 0.1
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var maxPriceCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = grayColor
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.dataSource = self
        cv.delegate = self
        cv.register(EstatePriceCell.self, forCellWithReuseIdentifier: self.pricesCellId)
        let firstIndex = IndexPath(row: 0, section: 0)
        cv.selectItem(at: firstIndex, animated: false, scrollPosition: UICollectionViewScrollPosition())
        return cv
    }()
    
    let maxPriceCollectionViewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize(width: -5, height: 5)
        view.layer.shadowRadius = 5
        view.layer.shadowOpacity = 0.1
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var realEstates = [RealEstate]() {
        didSet {
            sortBar.estatesCount = realEstates.count
        }
    }
    
    var fromPrices: [Int] = {
        var array = [Int]()
        for i in 5...16 {
            array.append(i*500000)
        }
        array.insert(0, at: 0)
        return array
    }()
    
    var toPrices: [Int] = {
        var array = [Int]()
        for i in 9...20 {
            array.append(i*500000)
        }
        array.insert(0, at: 0)
        return array
    }()
    
    var selectedToPrice: Int?
    var selectedFromPrice: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        ApiService.sharedService.fetchRealEstate() { estates in
            self.realEstates = estates
            
            DispatchQueue.main.async {
                self.feedCollectionView.reloadData()
            }
        }
    }
    
    func setupViews() {
        
        view.backgroundColor = .white
        
        view.addSubview(logoImageView)
        view.addConstraintsWithFormat("H:|[v0]|", views: logoImageView)
        view.addConstraintsWithFormat("V:|-20-[v0(66)]", views: logoImageView)
        
        view.addSubview(minPriceSelector)
        minPriceSelector.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        minPriceSelector.topAnchor.constraint(equalTo: logoImageView.bottomAnchor).isActive = true
        minPriceSelector.heightAnchor.constraint(equalToConstant: 48).isActive = true
        minPriceSelector.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/2).isActive = true
        
        view.addSubview(maxPriceSelector)
        maxPriceSelector.leftAnchor.constraint(equalTo: minPriceSelector.rightAnchor).isActive = true
        maxPriceSelector.topAnchor.constraint(equalTo: logoImageView.bottomAnchor).isActive = true
        maxPriceSelector.heightAnchor.constraint(equalToConstant: 48).isActive = true
        maxPriceSelector.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/2).isActive = true
        
        view.addSubview(sortBar)
        view.addConstraintsWithFormat("H:|[v0]|", views: sortBar)
        sortBar.topAnchor.constraint(equalTo: minPriceSelector.bottomAnchor).isActive = true
        sortBar.heightAnchor.constraint(equalToConstant: 38).isActive = true
        
        view.addSubview(feedCollectionView)
        view.addConstraintsWithFormat("H:|[v0]|", views: feedCollectionView)
        feedCollectionView.topAnchor.constraint(equalTo: sortBar.bottomAnchor).isActive = true
        feedCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.addSubview(minPriceCollectionViewContainer)
        minPriceCollectionViewContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        minPriceCollectionViewContainer.topAnchor.constraint(equalTo: minPriceSelector.bottomAnchor).isActive = true
        minPriceCollectionViewContainer.rightAnchor.constraint(equalTo: minPriceSelector.rightAnchor).isActive = true
        minPriceCollectionViewContainer.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        minPriceCollectionViewContainer.addSubview(minPriceCollectionView)
        minPriceCollectionViewContainer.addConstraintsWithFormat("H:|[v0]|", views: minPriceCollectionView)
        minPriceCollectionViewContainer.addConstraintsWithFormat("V:|[v0]|", views: minPriceCollectionView)
        
        view.addSubview(maxPriceCollectionViewContainer)
        maxPriceCollectionViewContainer.leftAnchor.constraint(equalTo: maxPriceSelector.leftAnchor).isActive = true
        maxPriceCollectionViewContainer.topAnchor.constraint(equalTo: maxPriceSelector.bottomAnchor).isActive = true
        maxPriceCollectionViewContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        maxPriceCollectionViewContainer.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        maxPriceCollectionViewContainer.addSubview(maxPriceCollectionView)
        maxPriceCollectionViewContainer.addConstraintsWithFormat("H:|[v0]|", views: maxPriceCollectionView)
        maxPriceCollectionViewContainer.addConstraintsWithFormat("V:|[v0]|", views: maxPriceCollectionView)
        
    }
    
    func handleMinPriceTapped() {
        minPriceCollectionViewContainer.isHidden = !minPriceCollectionViewContainer.isHidden
    }
    
    func handleMaxPriceTapped() {
        maxPriceCollectionViewContainer.isHidden = !maxPriceCollectionViewContainer.isHidden
    }
}

extension RealEstateFeedController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.isEqual(feedCollectionView) {
            return realEstates.count
        }
        return 10 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.isEqual(minPriceCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pricesCellId, for: indexPath) as! EstatePriceCell
            cell.price = fromPrices[indexPath.item]
            return cell
        }
        
        if collectionView.isEqual(maxPriceCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pricesCellId, for: indexPath) as! EstatePriceCell
            cell.price = toPrices[indexPath.item]
            return cell
        }
        
        if collectionView.isEqual(feedCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: estateCellId, for: indexPath) as! EstateCell
            cell.estate = realEstates[indexPath.item]
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath) as! EstateFooterCell
        footer.buttonHandler = {
            ApiService.sharedService.fetchRealEstate(withOffset: self.realEstates.count, priceFrom: self.selectedFromPrice ?? 0, priceTo: self.selectedToPrice ?? 0) { estates in
               
                self.realEstates.append(contentsOf: estates)
                
                DispatchQueue.main.async {
                    self.feedCollectionView.reloadData()
                }
            }
        }
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.isEqual(minPriceCollectionView){
            minPriceCollectionViewContainer.isHidden = true
            
            selectedFromPrice = fromPrices[indexPath.item]
            
            ApiService.sharedService.fetchRealEstate(priceFrom: selectedFromPrice!, priceTo: selectedToPrice ?? 0) { estates in
                self.realEstates = estates
                
                DispatchQueue.main.async {
                    self.feedCollectionView.reloadData()
                }
            }

        } else if collectionView.isEqual(maxPriceCollectionView)  {
            maxPriceCollectionViewContainer.isHidden = true
            
            selectedToPrice = toPrices[indexPath.item]
            
            ApiService.sharedService.fetchRealEstate(priceFrom: selectedFromPrice ?? 0, priceTo: selectedToPrice!) { estates in
                self.realEstates = estates
                
                DispatchQueue.main.async {
                    self.feedCollectionView.reloadData()
                }
            }
        }
    }
}

extension RealEstateFeedController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.isEqual(minPriceCollectionView) || collectionView.isEqual(maxPriceCollectionView) {
            return CGSize(width: UIScreen.main.bounds.width/2, height: 48)
        }
        
        return CGSize(width: UIScreen.main.bounds.width, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView.isEqual(feedCollectionView) {
            return CGSize(width: UIScreen.main.bounds.width, height: 76)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.isEqual(minPriceCollectionView) || collectionView.isEqual(maxPriceCollectionView) {
            return 0
        }
        
        return 10
    }
}

