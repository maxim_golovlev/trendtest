//
//  SortingBar.swift
//  TrendParcerTest
//
//  Created by Admin on 16.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class SortBar: UIView {
    
    let cellId = "cellId"
    
    let countLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Bold", size: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = grayColor
        cv.dataSource = self
        cv.delegate = self
        cv.register(SortCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsHorizontalScrollIndicator = false
        cv.bounces = false
        let firstIndex = IndexPath(row: 0, section: 0)
        cv.selectItem(at: firstIndex, animated: false, scrollPosition: UICollectionViewScrollPosition())
        return cv
    }()
    
    let sortTitles = ["по цене", "по району", "по метро", "по сроку сдачи"]
    
    var estatesCount: Int? {
        didSet {
            if let count = estatesCount {
                
                DispatchQueue.main.async {
                    self.countLabel.text = "\(count) объектов:"
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = grayColor
        
        addSubview(countLabel)
        countLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        countLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        
        addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: countLabel.rightAnchor, constant: 20).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SortBar: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SortCell
        cell.label.text = sortTitles[indexPath.item]
        return cell
    }
}

extension SortBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: self.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
