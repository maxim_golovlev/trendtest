//
//  SortCell.swift
//  TrendParcerTest
//
//  Created by Admin on 16.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class SortCell: BaseCell {
    
    override var isSelected: Bool {
        didSet {
            emphasLine.isHidden = isSelected ? false : true
        }
    }
    
    let emphasLine: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Roboto-Regular", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func setupViews() {
        
        backgroundColor = grayColor
        
        addSubview(label)
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(emphasLine)
        emphasLine.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 5).isActive = true
        emphasLine.leftAnchor.constraint(equalTo: label.leftAnchor).isActive = true
        emphasLine.rightAnchor.constraint(equalTo: label.rightAnchor).isActive = true
        emphasLine.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
}

